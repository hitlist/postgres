'use strict'

module.exports = class Transaction {
  constructor(db) {
    this.db = db
    this.client = null
  }

  async begin() {
    const client = await this.db.pool.connect()
    this.client = client
    try {
      await this.client.query('BEGIN')
    } catch (err) {
      throw err
    }
  }

  async commit() {
    this._checkTransaction()
    try {
      await this.client.query('COMMIT')
    } catch (err) {
      throw err
    } finally {
      this._release()
    }
  }

  async rollback() {
    this._checkTransaction()
    try {
      await this.client.query('ROLLBACK')
    } catch (err) {
      throw err
    } finally {
      this._release()
    }
  }

  async query({text, name, values}) {
    this._checkTransaction()
    return this.client.query({text, name, values})
  }

  async queryOne({text, name, values}) {
    this._checkTransaction()
    const result = await this.client.query({text, name, values})
    if (!result.rowCount) {
      const er = new Error('Unable to find record')
      er.code = 'ENOENT'
      throw er
    }
    return result.rows[0]
  }

  _checkTransaction() {
    if (!this.client) {
      throw new Error('Connection not acquired. Must call begin()')
    }
  }

  _release() {
    if (!this.client) return
    this.client.release()
    this.client = null
  }
}
