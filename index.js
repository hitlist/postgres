'use strict'

const assert = require('assert')
const EE = require('events')
const {Pool} = require('pg')
const Transaction = require('./transaction')

module.exports = class DB extends EE {
  constructor(opts) {
    super()
    opts = Object.assign({
      port: 5432
    , pool: {
        min: 2
      , max: 50
      , idleTimeoutMillis: 10000
      }
    }, opts)

    assert(opts.host, 'host is required')
    assert(opts.database, 'database is required')
    assert(opts.user, 'user is required')

    this.opts = opts

    this.database = opts.database
    this.pool = new Pool({
      database: opts.database
    , user: opts.user
    , password: opts.password
    , port: opts.port
    , host: opts.host
    , max: opts.pool.max
    , min: opts.pool.min
    , idleTimeoutMillis: opts.pool.idleTimeoutMillis
    })
  }

  async query({text, name, values}) {
    const client = await this.pool.connect()
    try {
      return client.query({text, name, values})
    } catch (err) {
      throw err
    } finally {
      client.release()
    }
  }

  async queryOne({text, name, values}) {
    const client = await this.pool.connect()
    try {
      const result = await client.query({text, name, values})
      if (!result.rowCount) {
        const er = new Error('Unable to find record')
        er.code = 'ENOENT'
        throw er
      }
      return result.rows[0]
    } catch (err) {
      throw err
    } finally {
      client.release()
    }
  }

  async close() {
    await this.pool.end()
  }

  async transaction() {
    const t = new Transaction(this)
    await t.begin()
    return t
  }
}
