'use strict'

const test = require('tap').test
const DB = require('../')

let db

test('DB', (t) => {
  t.throws(() => {
    new DB()
  }, /host is required/)

  t.throws(() => {
    new DB({
      host: 'localhost'
    })
  }, /database is required/)

  t.throws(() => {
    new DB({
      host: 'localhost'
    , database: 'poc'
    })
  }, /user is required/)

  db = new DB({
    host: process.env.CI ? 'postgres' : 'localhost'
  , database: process.env.POSTGRES_DB || 'poc'
  , user: process.env.POSTGRES_USER || 'postgres'
  , password: process.env.POSTGRES_PASSWORD || ''
  })

  t.end()
})

test('query error', (t) => {
  t.rejects(db.query({
    text: 'SELECT fdsafasdfdasf'
  , name: 'selectInvalidThingy'
  , values: []
  }))
  t.end()
})

test('query success', async (t) => {
  const res = await db.query({
    text: 'SHOW SERVER_VERSION'
  , name: 'showServerVersionQuery'
  , values: []
  })

  t.type(res, 'object')
  t.equal(res.command, 'SHOW', 'res.command === SHOW')
  t.equal(res.rows.length, 1, 'res.rows.length === 1')
  t.match(res.rows[0].server_version, /([\d]+)\.([\d]+)/)
  t.end()
})

const TEST_TABLE_NAME = 'node_helpdotcom_pg_test'

const createTableSQL = `
drop table if exists public."${TEST_TABLE_NAME}";
create table public."${TEST_TABLE_NAME}" (
  id serial primary key
  , name varchar(255) not null
)`

test('transaction', async (t) => {
  {
    const T = require('../transaction')
    const tx = new T(db)
    t.rejects(tx.query({}), /Connection not acquired/)
  }

  await db.query({
    text: createTableSQL
  , values: []
  })

  {
    const tx = await db.transaction()
    const sql = `insert into ${TEST_TABLE_NAME} (name) values ($1)`
    await tx.query({
      text: sql
    , values: ['evan']
    })

    const res = await tx.query({
      text: `select count(*) as count from ${TEST_TABLE_NAME}`
    })
    t.deepEqual(res.rows, [
      { count: '1' }
    ])

    await tx.rollback()

    {
      const res = await db.query({
        text: `select count(*) as count from ${TEST_TABLE_NAME}`
      })
      t.deepEqual(res.rows, [
        { count: '0' }
      ])
    }
  }

  {
    const tx = await db.transaction()
    const sql = `insert into ${TEST_TABLE_NAME} (name) values ($1)`
    await tx.query({
      text: sql
    , values: ['evan']
    })

    const res = await tx.query({
      text: `select count(*) as count from ${TEST_TABLE_NAME}`
    })
    t.deepEqual(res.rows, [
      { count: '1' }
    ])

    await tx.commit()

    const res2 = await db.query({
      text: `select count(*) as count from ${TEST_TABLE_NAME}`
    })
    t.deepEqual(res2.rows, [
      { count: '1' }
    ])
  }
})

test('db.queryOne', async (t) => {
  const sql = 'select table_name from information_schema.tables'
  t.rejects(db.queryOne({
    text: sql + ' where table_name = $1'
  , values: ['a_fake_table_name']
  }), /Unable to find record/)

  try {
    await db.queryOne({
      text: sql + ' where id = $1'
    , values: []
    })
    t.fail('should have thrown')
  } catch (err) {
    t.pass('error was thrown')
  }

  const res = await db.queryOne({
    text: sql + ' where table_name = $1'
  , values: [TEST_TABLE_NAME]
  })

  t.equal(res.table_name, TEST_TABLE_NAME)

  t.end()
})

test('tx.queryOne', async (t) => {
  const tx = await db.transaction()
  await tx.query({
    text: `insert into ${TEST_TABLE_NAME} (name) values ($1)`
  , values: ['biscuits']
  })

  const res = await tx.queryOne({
    text: `select * from ${TEST_TABLE_NAME} where name = $1`
  , values: ['biscuits']
  })

  t.match(res, {
    name: 'biscuits'
  , id: /\d+/
  })

  t.rejects(tx.queryOne({
    text: `select * from ${TEST_TABLE_NAME} where biscuits = $1`
  , values: []
  }))
  await tx.rollback()
})

test('cleanup', async (t) => {
  await db.close()
})
